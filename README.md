# [theokrueger.dev](https://theokrueger.dev/)

A personal webpage made using [SSGen](https://gitlab.com/theokrueger/ssgen)

Styled after Emacs and the deprecated [light-blue-theme.el](https://git.savannah.gnu.org/cgit/emacs.git/tree/etc/themes/light-blue-theme.el)

(https://theokrueger.dev/)
